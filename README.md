# README #

Devoxx France 2018 template

### What is this repository for? ###

This repository contains different templates for Devoxx France 2018. It's not a closed repository, you can also contribute and propose your presentation.

### What's include? ####

We offer the following supports : 
- Keynote v6 for Mac
- Powerpoint 
- Keynote 09 older version
- LibreOffice
- Reveal.js

### How do I contribute ###

If you want to create your own Devoxx France 2018, we'd like that you use our guidelines, fonts and logo so that we can offer a consolidated experience to our attendees.

If you want to publish your work : 

  - add your name to the contributors.txt file
  - create a pull request 
  - submit your PR and send me an email

### Devoxx France 2018 Fonts and Colors

### Fonts

#### Titles

For main title, we used Montserrat (https://www.google.com/fonts/specimen/Montserrat) 
You can use Montserrat semi-bold for titles.

#### Main text

For main text we use Open Sans, one of the Google Font, with variant Normal, 400

For the "computer fixed font" (R)evolution => we ise Ocr A Std Regulard.
You can also download this font here : 
http://fontsgeek.com/fonts/OCR-A-Std-Regular/download

#### Direct Google font link

You can use this link for Montserrat

(<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans' rel='stylesheet' type='text/css'>)[Google Font Montserrat]

#### Color chart 1

The main color this year is red, on black.

https://color.adobe.com/fr/cloud/aHR0cHM6Ly9jYy1hcGktYXNzZXRzLmFkb2JlLmlv/library/1eda1221-61b1-40cc-ae53-510f2590fdf9/theme/51ca1030-a962-4437-98c2-d62954e473e5/

The main Theme is based on the following colors :

  - main color, dark red, #7f1111
  - secondary color, light pink red blue, #FF1c4c
  - third color, red #FF1417
  - alternate color, accent, #DB1837
  - alternate color 2, #cc0c0c 

### What is the Devoxx France 2018 Theme?  

The logo was created by Nicolas Martignole. Based on the 7 digit number and like an iceberg, it's a mix of Revolution and Evolution, our main theme for 2018

## I want some nice images !

I suggest that you use Duotone and you pick images with the 2 main Devoxx colors :

https://duotone.shapefactory.co/?f=7f1111&t=ff1c4c&q=evolution

and also

https://duotone.shapefactory.co/?f=7f1111&t=ff1c4c&q=revolution

### What is the recommended format? 

Almost all presentations are recorded. Your presentation will be available few weeks after Devoxx France on the Youtube Devoxx channel. 

We recommend that you keep the 16/9 ratio format for optimal display.

This year we record your laptop video output directly and thus we won't need PDF or your slides after the presentation. Feel free to use any presentation technologies, but try to follow our guidelines and the color chart.

Devoxx France are not mandatory. If you prefer to use your own presentation, do it.

However please insert a first slide with the Devoxx France theme, then keep your presentation as it is. Always remember : your presentation is recorded. 

Last but not least, we also have a code of conduct on the Devoxx France web site.

Thanks

Nicolas Martignole
nicolas.martignole@devoxx.fr


